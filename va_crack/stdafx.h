// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

static void _dbg_printf(const char *format, ...)
{
#ifdef _DEBUG
    char buff[1024] = {0};
    va_list ap;
    va_start(ap, format);
    _vsnprintf_s(buff, 1024, format, ap);
    va_end(ap);

    char buff2[2048] = {0};

    sprintf_s(buff2, "[VA]: %s", buff);

    OutputDebugStringA(buff2);
#endif
}

// TODO: reference additional headers your program requires here
