// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

DWORD WINAPI InstallHook(LPVOID lParameter);

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
#ifdef _DEBUG
        MessageBox(NULL, NULL, NULL, MB_OK);
#endif
        InstallHook(NULL);
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}


#include <stdio.h>
#include <windows.h>
#include "Shlwapi.h"

HMODULE g_hVERSION = NULL;

#define VERSION_DEF_IMPORT(x, y) g_fp##x = GetProcAddress( g_hVERSION, y );

#define VERSION_BUILD_STUB(x) __declspec(naked) void x ( void ) { \
	__asm pushad \
	__asm call VERSIONInitializeImports \
	__asm popad \
	__asm jmp g_fp##x## \
}

FARPROC g_fpGetFileVersionInfoA;
FARPROC g_fpGetFileVersionInfoByHandle;
FARPROC g_fpGetFileVersionInfoExA;
FARPROC g_fpGetFileVersionInfoExW;
FARPROC g_fpGetFileVersionInfoSizeA;
FARPROC g_fpGetFileVersionInfoSizeExA;
FARPROC g_fpGetFileVersionInfoSizeExW;
FARPROC g_fpGetFileVersionInfoSizeW;
FARPROC g_fpGetFileVersionInfoW;
FARPROC g_fpVerFindFileA;
FARPROC g_fpVerFindFileW;
FARPROC g_fpVerInstallFileA;
FARPROC g_fpVerInstallFileW;
FARPROC g_fpVerLanguageNameA;
FARPROC g_fpVerLanguageNameW;
FARPROC g_fpVerQueryValueA;
FARPROC g_fpVerQueryValueW;

void VERSIONInitializeImports( void )
{
	if( g_hVERSION == NULL )
	{
		char szBuffer[ MAX_PATH ] = "";
		GetSystemDirectoryA( szBuffer, sizeof( szBuffer ) );
		strcat_s( szBuffer, "\\version.dll");
		g_hVERSION = LoadLibraryA( szBuffer );
		VERSION_DEF_IMPORT( GetFileVersionInfoA, "GetFileVersionInfoA" );
		VERSION_DEF_IMPORT( GetFileVersionInfoByHandle, "GetFileVersionInfoByHandle" );
		VERSION_DEF_IMPORT( GetFileVersionInfoExA, "GetFileVersionInfoExA" );
		VERSION_DEF_IMPORT( GetFileVersionInfoExW, "GetFileVersionInfoExW" );
		VERSION_DEF_IMPORT( GetFileVersionInfoSizeA, "GetFileVersionInfoSizeA" );
		VERSION_DEF_IMPORT( GetFileVersionInfoSizeExA, "GetFileVersionInfoSizeExA" );
		VERSION_DEF_IMPORT( GetFileVersionInfoSizeExW, "GetFileVersionInfoSizeExW" );
		VERSION_DEF_IMPORT( GetFileVersionInfoSizeW, "GetFileVersionInfoSizeW" );
		VERSION_DEF_IMPORT( GetFileVersionInfoW, "GetFileVersionInfoW" );
		VERSION_DEF_IMPORT( VerFindFileA, "VerFindFileA" );
		VERSION_DEF_IMPORT( VerFindFileW, "VerFindFileW" );
		VERSION_DEF_IMPORT( VerInstallFileA, "VerInstallFileA" );
		VERSION_DEF_IMPORT( VerInstallFileW, "VerInstallFileW" );
		VERSION_DEF_IMPORT( VerLanguageNameA, "VerLanguageNameA" );
		VERSION_DEF_IMPORT( VerLanguageNameW, "VerLanguageNameW" );
		VERSION_DEF_IMPORT( VerQueryValueA, "VerQueryValueA" );
		VERSION_DEF_IMPORT( VerQueryValueW, "VerQueryValueW" );

	}
}

VERSION_BUILD_STUB(GetFileVersionInfoA)
VERSION_BUILD_STUB(GetFileVersionInfoByHandle)
VERSION_BUILD_STUB(GetFileVersionInfoExA)
VERSION_BUILD_STUB(GetFileVersionInfoExW)
VERSION_BUILD_STUB(GetFileVersionInfoSizeA)
VERSION_BUILD_STUB(GetFileVersionInfoSizeExA)
VERSION_BUILD_STUB(GetFileVersionInfoSizeExW)
VERSION_BUILD_STUB(GetFileVersionInfoSizeW)
VERSION_BUILD_STUB(GetFileVersionInfoW)
VERSION_BUILD_STUB(VerFindFileA)
VERSION_BUILD_STUB(VerFindFileW)
VERSION_BUILD_STUB(VerInstallFileA)
VERSION_BUILD_STUB(VerInstallFileW)
VERSION_BUILD_STUB(VerLanguageNameA)
VERSION_BUILD_STUB(VerLanguageNameW)
VERSION_BUILD_STUB(VerQueryValueA)
VERSION_BUILD_STUB(VerQueryValueW)