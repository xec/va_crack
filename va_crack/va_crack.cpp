// va_crack.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdio.h>
#include "../MHook/mhook-lib/mhook.h"
#include <stdlib.h>

#define SEARCH_SIGNATURE(val, start, end, sigs) \
    if (!val) {            \
        for (int i = 0; i < _countof(sigs); i++) {  \
            val = find_address((char*)base_address + start, end, sigs[i]);  \
            if (val)    \
                break;  \
        }   \
    }

#define PATCH_MEMORY(address, newval, patchsize)    \
    VirtualProtect((LPVOID)address, patchsize, dwAttribute, &dwOldAttribute);    \
    memcpy((char *)address, newval, patchsize);  \
    VirtualProtect((LPVOID)address, patchsize, dwOldAttribute, NULL);    

template<typename T>
static int wildcmp(const T *string, const T *wild)
{
    const T *cp, *mp;
    while ((*string) && (*wild != '*')) {
        if ((*wild != *string) && (*wild != '?') && !(*wild == '#' && (*string >= '0' && *string <= '9')))
            return 0;
        wild++;
        string++;
    }

    while (*string) {
        if (*wild == '*') {

            if (!*++wild)
                return 1;
            mp = wild;
            cp = string + 1;
        } else if ((*wild == *string) ||
                   (*wild == '?') ||
                   (*wild == '#' && (*string >= '0' && *string <= '9'))) {
            wild++;
            string++;
        } else {
            wild = mp;
            string = cp++;
        }
    }
    while (*wild == '*') wild++;
    return !*wild;
}

/*
// 关键点：ASCII "VAX:ArmThread"

Signature 1:{1EE1C131}
1EDFCA61     83C4 08                    add esp,8
1EDFCA64     8B06                       mov eax,dword ptr ds:[esi]
1EDFCA66     8BCE                       mov ecx,esi
1EDFCA68     5E                         pop esi
1EDFCA69     FF60 68                    jmp dword ptr ds:[eax+68]

// 关键点：ASCII "ComSetup" 上面一个CALL

Signature 2:{1EE605D6}
1EE3EE26     83C0 10                    add eax,10
1EE3EE29     83C4 04                    add esp,4
1EE3EE2C     8945 EC                    mov dword ptr ss:[ebp-14],eax
1EE3EE2F     C745 FC 01000000           mov dword ptr ss:[ebp-4],1
1EE3EE36     E8 D5D3F4FF                call VA_X.1ED8C210
1EE3EE3B     85C0                       test eax,eax
1EE3EE3D     0F84 7A020000              je VA_X.1EE3F0BD

Signature 3:{1F2D7F30}

4c6963656e73653a20747269616c
// License:Full
4c6963656e73653a2046756c6c00
*/

char sig1[][255] ={
    "83c4088b068bce5eff60*",
    "83c4088b068b50688bce*",
};
//char sig2[] = "e8.{8}85c00f84.{8}8b.{10,14}0f84.{8}f605.{8}0174";
//char sig2[] = "e8????????85c00f84????????8b45??8378??000f84????????f605????????0174*";
char sig2[][255] = 
{
    "e8????????85c00f84????????8b??????????0f84????????f605????????0174*",
    "e8????????85c00f84????????8b????????????0f84????????f605????????0174*",
    "e8????????85c00f84????????8b??????????????0f84????????f605????????0174*",
};
char sig3[][255] = {
    "4c6963656e73653a20747269616c*"
};
ULONG32 sig1_address = 0, sig2_address = 0, sig3_address = 0;
bool patched = false;

char *bin2hex(char *bin,unsigned int bin_len)
{
    int pos = 0;
    int offset = 0;
    char *hex;
    hex = new char[bin_len * 2 + 1];
    memset(hex,0,bin_len * 2 + 1);
    while(pos < bin_len)
    {
        offset += sprintf(hex + offset,"%02x",(unsigned char)bin[pos]);
        pos++;
    }
    return hex;
}

ULONG32 find_address(char *base, size_t len, const char* signature)
{
    ULONG32 result = 0;

    if (base[0] == 0 && base[1] == 0)
        return 0;


    char* hex = bin2hex((char*)base, len);
    int hex_len = strlen(hex);
    char* addr = 0;
    for (addr = hex; addr < (hex + hex_len); addr += 2) {

        if (wildcmp<char>(addr, signature)) {
            result = (ULONG32)base + ((addr - hex) / 2);
            _dbg_printf("find address: %08x\n", result);
            break;
        }

        /*std::string binary =
            bin2hex((unsigned char *)addr, 4096);
        boost::smatch what;
        if (boost::regex_search(binary, what, reg)) {
            result = addr + (what[0].first - binary.begin()) / 2;
            _dbg_printf("find address: %08x\n", result);
            break;
        }*/
    }

    delete []hex;

    return result;
}

typedef LPVOID(WINAPI *_VirtualAlloc)(_In_opt_ LPVOID lpAddress,
                                      _In_ SIZE_T dwSize,
                                      _In_ DWORD flAllocationType,
                                      _In_ DWORD flProtect);

_VirtualAlloc real_VirtualAlloc = NULL;

LPVOID WINAPI Fake_VirtualAlloc(_In_opt_ LPVOID lpAddress, _In_ SIZE_T dwSize,
                                _In_ DWORD flAllocationType,
                                _In_ DWORD flProtect)
{
    HMODULE base_address = NULL;
    ULONG image_size = 0xfffff;

    if (!patched) {
        DWORD search_time = GetTickCount();
        base_address = GetModuleHandle(L"VA_X.dll");
        if (base_address && !sig1_address && !sig2_address && !sig3_address) {
            
            _dbg_printf("VA_X base address: %08x\n", base_address);

            SEARCH_SIGNATURE(sig1_address, 0x100000, 0x30000, sig1);
            SEARCH_SIGNATURE(sig2_address, 0x100000, 0x8ffff, sig2);
            SEARCH_SIGNATURE(sig3_address, 0x500000, 0xfffff, sig3);

            _dbg_printf(
                "Signature 1 address: %08x, Signature 2 address: %08x, Signature 3 address: %08x\n",
                sig1_address, sig2_address, sig3_address);
        }

        if (sig1_address && sig2_address && sig3_address) {

            _dbg_printf("find signature use time: %d\n", GetTickCount() - search_time);
            DWORD dwAttribute = PAGE_EXECUTE_READWRITE;
            DWORD dwOldAttribute = 0;
            /*
            1EDFCA55     40                         inc eax
            1EDFCA56     90                         nop
            */
            unsigned char patch1[] = { 0x40, 0x90 };
            ULONG patch_address1 = sig1_address - 12;

            _dbg_printf("patch address 1: %08x\n", patch_address1);
            PATCH_MEMORY(patch_address1, patch1, sizeof(patch1));
            /*
            1EE3EE36     E8 D5D3F4FF                call VA_X.1ED8C210
            */

            unsigned char patch2[] = { 0xb8, 0x01, 0x00, 0x00, 0x00 };
            ULONG patch_address2 = sig2_address;
            _dbg_printf("patch address 2: %08x\n", patch_address2);
            PATCH_MEMORY(patch_address2, patch2, sizeof(patch2));


            /*
            4c6963656e73653a2046756c6c00 // License:Full
            */

            unsigned char patch3[] = { 0x4c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65, 0x3a, 0x20, 0x46, 0x75, 0x6c, 0x6c, 0x00 };
            ULONG patch_address3 = sig3_address;
            _dbg_printf("patch address 3: %08x\n", patch_address3);
            PATCH_MEMORY(patch_address3, patch3, sizeof(patch3));

            patched = true;
        }
    }

    return real_VirtualAlloc(lpAddress, dwSize, flAllocationType, flProtect);
}

DWORD WINAPI InstallHook(LPVOID lParameter)
{

    real_VirtualAlloc = (_VirtualAlloc)GetProcAddress(
        LoadLibrary(L"kernel32.dll"), "VirtualAlloc");

    if (real_VirtualAlloc) {
        Mhook_SetHook((PVOID *)&real_VirtualAlloc, Fake_VirtualAlloc);
    } else {
        _dbg_printf("install hook failed\n");
    }

    return 0;
}